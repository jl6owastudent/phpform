<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title></title>
		<link rel="stylesheet" href="">
	</head>
	<body>
		<?php
		echo "<h1>New user</h1>";
		$fields=['firstname','lastname','email','password'];
		function GetRequestField($field)
		{
			$result = $_REQUEST[$field];
			return $result;
		}
        	for ($i=0; $i <count($fields) ; $i++) {
			$temp[] = GetRequestField($fields[$i]);
			if (strlen($temp[$i])>0)
			echo "<p>$fields[$i] : $temp[$i]</p>";
			else
				echo "<p>$fields[$i] : is empty</p>";
			}
        require_once ('dbconnection.php');
        $validated=True;
        for ($i=0; $i<4; $i++)
        {
            if (strlen($temp[$i])<=0 && strlen($temp[$i]>=255)) {
                $validated = False;
            }
        }
        for ($i=0; $i<2; $i++) {
            if (!preg_match("/^[a-zA-Z-' ]*$/", $temp[$i])) {
                $validated=False;
            }
        }
        if (!filter_var($temp[2], FILTER_VALIDATE_EMAIL)) {
            $validated=False;
        }
        if ($validated) {
            $pwd=hash('sha256',$temp[3]);
            $temp[3]=$pwd;
            $sql = "INSERT INTO users (firstname, lastname, email, password) VALUES (?,?,?,?)";
            $x = $conn->prepare($sql);
            $x->bindparam(1,$temp[0]);
            $x->bindparam(2,$temp[1]);
            $x->bindparam(3,$temp[2]);
            $x->bindparam(4,$temp[3]);
            if ($x->execute() === TRUE) {
                $_SESSION['msg'] = 'success';
            }
            }
        else {
                $_SESSION['msg'] = 'failure';
                $_SESSION['old_firstname'] = $_REQUEST['firstname'];
                $_SESSION['old_lastname'] = $_REQUEST['lastname'];
                $_SESSION['old_email'] = $_REQUEST['email'];

            }
            header('Location: http://mysite.test');

		?>
	</body>
</html>