<?php
require_once('dbconnection.php');
session_start();

$fields = ['userid', 'firstname', 'lastname', 'email'];
function GetRequestField($field)
{
    $result = $_REQUEST[$field];
    return $result;
}
for ($i = 0; $i < count($fields); $i++) {
    $temp[] = GetRequestField($fields[$i]);
}

$uploadedfile = 'storage/' . $_FILES['profilepic']['name'];
if (move_uploaded_file($_FILES['profilepic']['tmp_name'], $uploadedfile)) ;
{
    $sql = "UPDATE users SET profilepic=? WHERE userid=?";
    $x = $conn->prepare($sql);
    $x->bindparam(1, $uploadedfile);
    $x->bindparam(2, $temp[0]);
    $x->execute();
}
$sql = "UPDATE users SET firstname=?, lastname=?, email=? WHERE userid=?";
$x = $conn->prepare($sql);
$x->bindparam(1, $temp[1]);
$x->bindparam(2, $temp[2]);
$x->bindparam(3, $temp[3]);
$x->bindparam(4, $temp[0]);
$sql = "SELECT email FROM users WHERE (email = ? AND userid <> ?)";
$x = $conn->prepare($sql);
$x->bindparam(1, $temp[3]);
$x->bindparam(2, $temp[0]);
$x->execute();
if ($x->rowCount() > 0) {
    $_SESSION['msg'] = 'update_fail';
} else {
    if ($x->execute() === TRUE) {
        $_SESSION['userid'] = $temp[0];
        $_SESSION['firstname'] = $temp[1];
        $_SESSION['lastname'] = $temp[2];
        $_SESSION['email'] = $temp[3];
        $_SESSION['profilepic'] = $uploadedfile;
        $_SESSION['msg'] = 'update_success';
    } else {
        echo "Error updating record: " . $conn->error;
    }

}
header('Location: http://mysite.test/profile.php');
