<?php
require_once ('dbconnection.php');
session_start();
echo $_SESSION['userid'];
$uid = $_SESSION['userid'];
$sql = "DELETE FROM sessions  WHERE (userid = ?)";
$x = $conn->prepare($sql);
$x->bindparam(1,$uid);
$x->execute();
session_unset();
session_destroy();
header('Location: http://mysite.test');
