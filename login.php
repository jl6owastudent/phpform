<?php
session_start();
require_once ('dbconnection.php');
$sid = session_id();
$email = $_REQUEST['email'];
$password = hash('sha256', $_REQUEST['password']);
$sql = "SELECT * FROM users WHERE (email = ?)";
$x = $conn->prepare($sql);
$x->bindparam(1,$email);
$x->execute();
$user = $x->fetch(PDO::FETCH_ASSOC);
if ($user["email"] == $email && $user["password"] == $password) {
    $userid = $user["userid"];
    $expirationdate = time() + 1800;
    $expired=0;
    $sql = "SELECT * FROM sessions WHERE (sessionid = ?)";
    $x = $conn->prepare($sql);
    $x->bindparam(1,$sid);
    $x->execute();
    if ($x->rowCount() > 0) {
        $sql = "UPDATE sessions SET expirationdate=?  WHERE userid=?";
        $x = $conn->prepare($sql);
        $x->bindparam(1,$expirationdate);
        $x->bindparam(2,$userid);
    }
    else {
        $sql = "INSERT INTO sessions (userid, sessionid,expirationdate, expired) VALUES (?,?,?,?)";
        $x = $conn->prepare($sql);
        $x->bindparam(1, $userid);
        $x->bindparam(2, $sid);
        $x->bindparam(3, $expirationdate);
        $x->bindparam(4, $expired);
    }
    if ($x->execute() === TRUE)
{
        $_SESSION['userid']=$user["userid"];
        $_SESSION['firstname']=$user["firstname"];
        $_SESSION['lastname']=$user["lastname"];
        $_SESSION['email']=$user["email"];
        $_SESSION['profilepic']=$user["profilepic"];
        header('Location: http://mysite.test/profile.php');
}
} else {
    echo 'You are not logged in';
    $_SESSION['msg'] = 'login_fail';
    $_SESSION['old_email'] = $email;
    header('Location: http://mysite.test');
}