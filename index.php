<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>
            My site test
		</title>
        <link rel="stylesheet" href="\style\style.css">
	</head>
	<body>
		<div class="container">
				<div class="content">
						<?php
						if (isset($_SESSION['msg'])) {
                            if ($_SESSION['msg'] == 'success') {
                                $message = '<div class="row"><div class="success"><p>User is created sucessfully you can log in.</p></div></div>';
                                $_SESSION['msg'] = '';
                                echo $message;
                            }
                            if ($_SESSION['msg'] == 'failure') {
                                $message = '<div class="row"><div class="failure"><p>Something went wrong, please check all the fields</p></div></div>';
                                $_SESSION['msg'] = '';
                                echo $message;
                            }
                        }
						?>
					<div class="row">
						<button type="button" class="main-size buttons font" onclick="showsignup()">Sign Up</button>
						<button type="button" class="main-size buttons font" onclick="showlogin()">Log In</button>
					</div>
					<div id="signup">
                        <p class="big-size center-text font">Sign Up for Free</p>
						<form action="/createuser.php" method="post" accept-charset="utf-8">
						<div class="content">
							<div class="row">
								<input class="main-size font" type="text" name="firstname" value="<?php if (isset($_SESSION['old_firstname'])) echo $_SESSION['old_firstname'] ?>" placeholder="First Name*">
								<input class="main-size font" type="text" name="lastname" value="<?php if (isset($_SESSION['old_lastname'])) echo $_SESSION['old_lastname'] ?>" placeholder="Last Name*">
							</div>
						    	<input class="main-size font" type="email" name="email" value="<?php if (isset($_SESSION['old_email'])) echo $_SESSION['old_email'] ?>" placeholder="Email Address*">
							    <input class="main-size font" type="password" name="password" value="" placeholder="Set a Password*">
							<input type="submit" class="big-size font buttons"  style="background-color: #117a60;" value="Get Started">
						</div>
					</form>
                    </div>
					<div id="login">
						<form action="/login.php" method="post" accept-charset="utf-8">
						    <div class="content">
                                <?php
                                if (isset($_SESSION['msg'])) {
                                    if ($_SESSION['msg'] == 'login_fail') {
                                        $message = '<div class="row"><div class="failure"><p>Login failed</p></div></div>';
                                        $_SESSION['msg'] = '';
                                        echo $message;
                                    }
                                }
                                ?>
							    <input class="main-size font" type="email" name="email" value="<?php if (isset($_SESSION['old_email'])) echo $_SESSION['old_email'] ?>" placeholder="Email Address*">
							    <input class="main-size font" type="password" name="password" value="" placeholder="Password*">
							    <input type="submit" class="big-size font buttons"  style="background-color: #117a60;" value="Log-In">
					    	</div>
				    	</form>
					</div>
				</div>
		</div>

        <?php
        session_unset();
        ?>
	</body>
</html>
<script type="text/javascript">
	function showlogin() {
			var	signup=document.getElementById("signup");
		signup.style.display="none";
		login=document.getElementById("login");
		login.style.display="block";
	}
		function showsignup() {
			var	signup=document.getElementById("signup");
		signup.style.display="block";
		login=document.getElementById("login");
		login.style.display="none";
	}
</script>