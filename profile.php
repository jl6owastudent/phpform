<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="\style\style.css">
</head>
<body>
<div class="container">
        <?php
        session_start();
        require_once ('dbconnection.php');
        require_once ('class.php');
        if (isset($_SESSION['userid'])) {
            $sid = session_id();
            $logged = false;
            $sql = "SELECT * FROM sessions WHERE (sessionid = ?)";
            $x = $conn->prepare($sql);
            $x->bindparam(1,$sid);
            $x->execute();
            if ($x->rowCount() > 0) {
                $user = $x->fetch(PDO::FETCH_ASSOC);
                $userid = $user["userid"];
                $currenttime = time();
                $expirationdate=$user["expirationdate"];
                if ($currenttime < $user["expirationdate"]) {
                    $t = new User();
                    $t->set_values($_SESSION['firstname'],$_SESSION['lastname'],$_SESSION['email'],$_SESSION['profilepic']);
                    $logged = true;
                } else {
                    $logged = false;
                }
            }
        }
        if ($logged==false) {
            session_unset();
            header('Location: http://mysite.test');
        }
        ?>
    <div class="content">
        <?php
        if (isset($_SESSION['msg'])) {
            if ($_SESSION['msg'] == 'update_fail') {
                $message = '<div class="row"><div class="failure"><p>Something went wrong, please check all the fields</p></div></div>';
                $_SESSION['msg'] = '';
                echo $message;
            }
            if ($_SESSION['msg'] == 'update_success') {
                $message = '<div class="row"><div class="success"><p>Information sucessfully updated</p></div></div>';
                $_SESSION['msg'] = '';
                echo $message;
            }
        }
        ?>
        <div class="row">
            <p class="big-size font text-center">Welcome back!</p>
            <img class="avatar" src="<?php echo $t->read_value('profilepic') ?>">
        </div>
        <p class="main-size font text-center">Here is your profile information:</p>
        <form action="/updateuser.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
            <div class="content">
            <p class="main-text font">First name:</p>
            <input type="hidden" name="userid" value="<?php echo $_SESSION["userid"] ?>">
            <input class="main-size font" type="text" name="firstname" value="<?php echo $t->read_value('firstname') ?>"  placeholder="First Name*">
            <p class="main-text font">Last name:</p>
            <input class="main-size font" type="text" name="lastname" value="<?php echo $t->read_value('lastname') ?>"  placeholder="Last Name*">
            <p class="main-text font">Email:</p>
            <input class="main-size font" type="eamil" name="email" value="<?php echo $t->read_value('email') ?>"  placeholder="Email*">
            <input type="file" name="profilepic" accept="image/*" class="main-text font">
            <div class="row">
            <button type="submit" class="main-size buttons font">Update information</button>
            <a href="/logout.php" class="main-size buttons font">Log-out</a>
           </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>



